# Teste de API SpaceX com integração CI/CD


## Passo a passo para realizar o deploy no gitlab

### Tutorial youtube

Neste tutorial é ensinado como foi montado a pipe.

[https://www.youtube.com/watch?v=H0WiDqhDIOs](https://www.youtube.com/watch?v=H0WiDqhDIOs)

### Imagens docker postmanlabs

Link para consultar as imagens disponível para utilizar.

[postmanlabs/newman](https://github.com/postmanlabs/newman/tree/develop/docker/)

### arquivo gitci

```yaml
stages:
- test

api-test:
  stage: test
  image: 
    name: postman/newman_alpine33
    entrypoint: [""]
  script:
    - newman --version
    - npm install -g newman-reporter-html
    - newman run SpaceX.postman_collection.json -e SpaceX.postman_environment.json --reporters cli,html --reporter-html-export report.html

  artifacts:
    when: always
    paths:
      - report.html
```

